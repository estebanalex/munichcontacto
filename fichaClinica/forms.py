from django import forms
from .models import FichaRegistrada

class RegModelForm(forms.ModelForm):
    class Meta:
        model = FichaRegistrada
        campos = ["enfermedadDetectada","tratamiento","fechaDeEnfermedad","fechaTerminoTratamiento","resultadoTratamiento"]
        exclude = ()
