from django.shortcuts import render
from .forms import RegModelForm
from .models import FichaRegistrada
# Create your views here.
def registroFicha(request):
    form = RegModelForm(request.POST or None)
    titulo = "Bienvenido"
    if request.user.is_authenticated():
        titulo = "Bienvenido %s" %(request.user)

    contexto = {
        "formulario_registro_ficha": form,
        "titulo": titulo,
    }

    if form.is_valid():
        #form_data = form.cleaned_data
        #enfermedadDetectada2 = form_data.get("enfermedadDetectada")
        #tratamiento2 = form.data.get("tratamiento")
        #fechaTerminoTratamiento2 = form.data.get("fechaTerminoTratamiento")
        #resultadoTratamiento2 = form.data.get("resultadoTratamiento")
        #objeto = FichaRegistrada.objects.create(enfermedadDetectada = enfermedadDetectada2, tratamiento = tratamiento2, fechaTerminoTratamiento = fechaTerminoTratamiento2, resultadoTratamiento = resultadoTratamiento2)
        instancia = form.save(commit=False)
        print(instancia)

        contexto = {
            "titulo": "Has registrado la ficha%s!" % (instancia.idFichaClinica),
        }
    return render(request, "registroFicha.html", contexto)