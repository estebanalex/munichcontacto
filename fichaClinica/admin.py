from django.contrib import admin
from fichaClinica.models import *
from .forms import RegModelForm
# Register your models here.

class AdminFichaRegistrada(admin.ModelAdmin):
    list_display = ["idFichaClinica","fechaDeEnfermedad","fechaTerminoTratamiento","resultadoTratamiento"]
    form = RegModelForm
    list_filter = ["fechaDeEnfermedad"]
    search_fields = ["idFichaClinica"]

    class Meta:
        model = FichaRegistrada

admin.site.register(FichaRegistrada, AdminFichaRegistrada)