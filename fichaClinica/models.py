from django.db import models

# Create your models here.
class FichaRegistrada(models.Model):
    idFichaClinica = models.AutoField(primary_key=True, null=False)
    enfermedadDetectada = models.CharField(max_length=50, blank=False, null=False)
    tratamiento = models.CharField(max_length=100, blank=False, null=False)
    fechaDeEnfermedad = models.DateField(auto_now=False, auto_now_add=True)
    fechaTerminoTratamiento = models.DateField(auto_now=False, auto_now_add=False)
    resultadoTratamiento = models.CharField(max_length=100, blank=False, null=False)

    def __int__(self):
        return self.idFichaClinica
