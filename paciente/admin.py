from django.contrib import admin
from paciente.models import *
from .forms import RegModelForm
# Register your models here.

class AdminPacienteRegistrado(admin.ModelAdmin):
    list_display = ["rutPaciente","nombrePaciente","edadIngreso","edadActual","fechaIngreso"]
    form = RegModelForm
    list_filter = ["fechaIngreso"]
    search_fields = ["rutPaciente","nombrePaciente"]


admin.site.register(PacienteRegistrado, AdminPacienteRegistrado)
