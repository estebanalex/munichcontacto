# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-15 18:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PacienteRegistrado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombrePaciente', models.CharField(max_length=100)),
                ('rutPaciente', models.IntegerField()),
                ('edadIngreso', models.IntegerField()),
                ('edadActual', models.IntegerField()),
                ('fechaIngreso', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
