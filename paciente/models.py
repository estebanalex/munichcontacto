from django.db import models

# Create your models here.
class PacienteRegistrado(models.Model):
    nombrePaciente = models.CharField(max_length=100, blank=False, null=False)
    rutPaciente = models.IntegerField(blank=False, null=False)
    edadIngreso = models.IntegerField()
    edadActual = models.IntegerField()
    fechaIngreso = models.DateField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.nombrePaciente