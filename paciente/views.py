from django.shortcuts import render
from .forms import RegModelForm
from .forms import ContactForm

# Create your views here.
def registroPaciente(request):
    form = RegModelForm(request.POST or None)
    titulo = "Bienvenido"
    if request.user.is_authenticated():
        titulo = "Bienvenido %s" %(request.user)

    contexto = {
        "formulario_registro_paciente": form,
        "titulo": titulo,
    }

    if form.is_valid():
       #form_data = form.cleaned_data
       #nombrePaciente2 = form.data.get("nombrePaciente")
       #rutPaciente2 = form.data.get("rutPaciente")
       #edadIngreso2 = form.data.get("edadIngreso")
       #edadActual2 = form.data.get("edadActual")
       #fechaIngreso2 = form.data.get("fechaIngreso")
       #objeto = PacienteRegistrado.objects.create(nombrePaciente = nombrePaciente2, rutPaciente = rutPaciente2, edadIngreso = edadIngreso2, edadActual = edadActual2, fechaIngreso = fechaIngreso2)
       instancia = form.save(commit=False)
       instancia.save()
       print(instancia)

       contexto = {
           "titulo": "Gracias %s!" %(instancia.nombrePaciente),
       }

    return render(request, "registroPaciente.html", contexto)

def contacto(request):
    form = ContactForm(request.POST or None)

    if form.is_valid():
        print(form.cleaned_data)
        formulario_nombre = form.cleaned_data.get("nombre")
        formulario_correoElectronico = form.cleaned_data("correoElectronico")
        formulario_mensaje = form.cleaned_data("mensaje")
        print(nombrePaciente,correoElectronico,mensaje)
        asunto = "Formulario de contacto - web"
        email_from = settings.EMAIL_HOST_USER
        email_to = email_from,"esteban.machuca@virginiogomez.cl"
        email_mensaje = "Enviado por %s - Correo: %s - Mensaje %s" %(formulario_nombre,formulario_correoElectronico,formulario_mensaje)
        send_mail (asunto,email_from,email_to,fail_silently=False)

        for key in form.cleaned_data:
            print(key)
            print(form.cleaned_data.get(key))

        for key, value in form.cleaned_data.items():
            print(key, value)

        contexto = {
        "contacto": form,
    }
    return render(request, "contacto.html",contexto)
