from django import forms
from .models import PacienteRegistrado

class RegModelForm(forms.ModelForm):
    class Meta:
        model = PacienteRegistrado
        campos = ["nombrePaciente","rutPaciente","edadIngreso","edadActual"]
        exclude = ()

class ContactForm(forms.Form):
    nombrePaciente = forms.CharField()
    correoElectronico = forms.EmailField()
    mensaje = forms.CharField(widget=forms.Textarea)